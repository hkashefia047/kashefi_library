package view;

import Controller.UserController;
import Service.BookService;

import java.util.Scanner;

public  class Menus {
    UserController userController;
    BookService bookService;
    Scanner in;

    public Menus() {
        userController = new UserController();
        bookService = new BookService();
        in = new Scanner(System.in);
    }

    public void showMainMenu() throws Exception {
        System.out.println("Do you already have account? Y/N");
        String answer=in.next();
        if (answer.equals("Y")||answer.equals("y")){
            userController.login();
        }else if (answer.equals("N")||answer.equals("n")){
            userController.registerUser();
        }

    }

}