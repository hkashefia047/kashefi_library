package Controller;

import Service.BookService;
import models.Book;

import java.util.Scanner;

public class BookController {
    public static int ChosenBookId;
    public static boolean ChosenBookStatus;
    public static long ChosenBookPrice;
    public static long ChosenBookPenalty;
    Scanner in;
    BookService bookService;

    public BookController() {
        in = new Scanner(System.in);
        bookService = new BookService();
    }


    public static long getChosenBookPenalty() {
        return ChosenBookPenalty;
    }

    public static void setChosenBookPenalty(long chosenBookPenalty) {
        ChosenBookPenalty = chosenBookPenalty;
    }

    public static long getChosenBookPrice() {
        return ChosenBookPrice;
    }

    public static void setChosenBookPrice(long chosenBookPrice) {
        ChosenBookPrice = chosenBookPrice;
    }

    public static boolean isChosenBookStatus() {
        return ChosenBookStatus;
    }

    public static void setChosenBookStatus(boolean chosenBookStatus) {
        ChosenBookStatus = chosenBookStatus;
    }

    public static int getChosenBookId() {
        return ChosenBookId;
    }

    public static void setChosenBookId(int chosenBookId) {
        ChosenBookId = chosenBookId;
    }

    public void insertBookData() {
        Book book = new Book();
        System.out.println("Please Insert Book information:");
        System.out.println("-------------------------------");

        System.out.println("please Enter Book Title:");
        book.setTitle(in.next());

        System.out.println("please Enter Book Subject:");
        book.setSubject(in.next());

        System.out.println("please Enter ISBN:");
        book.setIsbn(in.nextLong());

        book.setStatus(true);

        System.out.println("please Enter the book price:");
        Long price = in.nextLong();
        book.setPrice(price);

        System.out.println("Please Enter the penalty:");
        book.setPenalty(in.nextLong());

        bookService.insert(book);
    }

    public void readBookData() {
        bookService.readBooks();
    }

    public void SearchByName() throws Exception {
        System.out.println("Please Enter book Title that you want to borrow:");
        bookService.SearchByName(in.next());
    }
}
