package Controller;

import DAO.BorrowDAO;
import Service.BookService;
import Service.BorrowService;
import Service.UserService;
import models.Book;
import models.Borrow;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Scanner;

public class BorrowController {
    public static Date CurrentBorrowEndDate;
public static long calculatedPenaltyBudget;
    BorrowService borrowService;
    BorrowDAO borrowDAO;
    BookController bookController;
    Borrow b;
    Scanner in;
    public BorrowController() {
        borrowDAO = new BorrowDAO();
        bookController = new BookController();
        b = new Borrow();
        borrowService = new BorrowService();
        in = new Scanner(System.in);
    }

    public static long getCalculatedPenaltyBudget() {
        return calculatedPenaltyBudget;
    }

    public static void setCalculatedPenaltyBudget(long calculatedPenaltyBudget) {
        BorrowController.calculatedPenaltyBudget = calculatedPenaltyBudget;
    }

    public static Date getCurrentBorrowEndDate() {
        return CurrentBorrowEndDate;
    }

    public static void setCurrentBorrowEndDate(Date currentBorrowEndDate) {
        CurrentBorrowEndDate = currentBorrowEndDate;
    }

    //in this method user can borrow desire book
    public void start_Borrowing() throws Exception {

        b.setUSERID(UserController.getCurrentUserId());
        bookController.SearchByName();
        if (BookController.isChosenBookStatus()) {
            b.setBOOKID(BookController.getChosenBookId());

            b.setStart(CurrentDate());

            String ending = FinishTimeDeadline();
            b.setEnd(ending);
            borrowService.insertBarrowData(b);

        } else {
            System.out.println("Sorry this Book is not availabe");
        }

    }

    //this method is check return date and update budget for user
    public long UpdatingUserBudget() {
        long updateBudget = updatingBudgetOperation();
        return updateBudget;

    }

    //this method update new userBudget After Borrowing Book;
    private long updatingBudgetOperation() {
        long UserBudget = UserController.getCurrentUserBudget();
        long BookPrice = BookController.getChosenBookPrice();
        long newBudget = UserBudget - BookPrice;
        return newBudget;
    }

    //this method update new userBudget After Borrowing Book date is expired;
    public long updatingBudgetWithPenalty(String name) throws Exception {
        new BookService().SearchByName(name);
        System.out.println("Calculating DeadLine Penalty ");
        long UserBudget = UserController.getCurrentUserBudget();
        long BookPrice = BookController.getChosenBookPrice();
        long BookPenalty = BookController.getChosenBookPenalty();
        long newBudget = UserBudget - (BookPrice + BookPenalty);
        System.out.println("the penalty you will pay: " + (BookPrice + BookPenalty));
        return newBudget;
    }


    //add desire time to current time for declare finishTime RedLine
    private String FinishTimeDeadline() {
        LocalDateTime datetime1 = LocalDateTime.now();
        LocalDateTime datetime2 = datetime1.plusDays(2);
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        return datetime2.format(format);
    }

    //Getting current Time
    private String CurrentDate() {
        LocalDateTime datetime1 = LocalDateTime.now();

        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        return datetime1.format(format);
    }

    public void readBorrowTable() {
        borrowService.read();
    }


    public void returningBook() throws Exception {
        System.out.println("\n\nplease Enter book title for return:");
        String bookTitle = in.next();

        Date ending = borrowService.borrowDeadLine(bookTitle);
        Date current = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(CurrentDate());
        if (current.after(ending)) {
           long penalty= updatingBudgetWithPenalty(bookTitle);
           setCalculatedPenaltyBudget(penalty);

            UserService us = new UserService();
            us.updateWithPenalty();
        }
        borrowService.deleteRecord(bookTitle);
        int bookId = new BookService().gettingIdByName(bookTitle);
        BookController.setChosenBookId(bookId);
        new BookService().updateReturnBookStatus();

    }


}
