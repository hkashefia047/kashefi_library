package Controller;

import Security.PassHash;
import Service.BookService;
import Service.UserService;
import models.User;
import view.Menus;

import java.sql.Timestamp;
import java.util.Scanner;

public class UserController {

    public static String MyCurrentUser;
    public static int currentUserId;
    public static long currentUserBudget;

    BorrowController borrowController;
    UserService userService;
    BookService bookService;
    Scanner in;

    public UserController() {
       borrowController=new BorrowController();
        userService = new UserService();
        bookService = new BookService();
        in = new Scanner(System.in);
    }

    public static long getCurrentUserBudget() {
        return currentUserBudget;
    }

    public static void setCurrentUserBudget(long currentUserBudget) {
        UserController.currentUserBudget = currentUserBudget;
    }

    public static int getCurrentUserId() {
        return currentUserId;
    }

    public static void setCurrentUserId(int currentUserId) {
        UserController.currentUserId = currentUserId;
    }

    public static String getMyCurrentUser() {
        return MyCurrentUser;
    }

    public static void setMyCurrentUser(String myCurrentUser) {
        MyCurrentUser = myCurrentUser;
    }

    public void updateUserTable() {
        userService.update();
    }


    public void registerUser() throws Exception {

        User user = new User();

        System.out.println("Register is Begin");
        System.out.println("-----------------");

        System.out.println("please Enter fullName:");
        String fullname = in.next();
        user.setFull_name(fullname);

        System.out.println("please Enter Password:");

        user.setPassword(PassHash.getMd5(in.next()));

        Timestamp created_at = new Timestamp(System.currentTimeMillis());
        user.setCreated_at(created_at);

        System.out.println("please Enter your National Code:");
        String nationalcode = in.next();
        user.setNational_code(nationalcode);


        user.setStatus(true);
        System.out.println("please Enter your Budget:");
        long budget = in.nextLong();
        user.setBudget(budget);

        userService.insert(user);
        System.out.println("registeration is successfuly done");
        Menus menus = new Menus();
        menus.showMainMenu();
    }

    public void readUsersData() {
        userService.read();
    }

    public void login() throws Exception {

        System.out.println("Please Enter UserName:");
        String username = in.next();

        System.out.println("Please Enter Password:");
        String password = in.next();

        userService.login(username, password);

        System.out.println("Press (1) to <<Borrow Book>>\tpress (2) to <<return book>>\tpress (3) to <<donate Book>>:");
        String option=in.next();
        if (option.equals("1")) {

            bookService.readBooks();
            System.out.println();
            borrowController.start_Borrowing();
            updateUserTable();
            System.out.println("\n Here is Information about your Borrows:\n");
            borrowController.readBorrowTable();

        }else if (option.equals("2")) {
            System.out.println("\nHere is Information about your Borrows:\n");
            borrowController.readBorrowTable();
            borrowController.returningBook();

        }else if (option.equals("3")){
            new BookController().insertBookData();
        }else{
            System.out.println("invalid key");
        }

    }

}
