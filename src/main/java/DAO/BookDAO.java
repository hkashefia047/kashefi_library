package DAO;

import Controller.BookController;
import Controller.UserController;
import models.Book;

import java.sql.*;

public class BookDAO {

    private static final String booksTable = "CREATE TABLE IF NOT EXISTS Book(id INT PRIMARY KEY" +
            " ,title VARCHAR" +
            " ,subject VARCHAR" +
            " ,isbn INT8" +
            ",status BOOLEAN NOT NULL CHECK (status IN (0,1)))" +
            " ,price INT8" +
            ",penalty INT8)";

    private static final String QUERY = "select  * from book ";
    private static final String SearchByName = "select  id,status,price,penalty from book where title=? ";
    private static final String GettingTitleId = "select  id from book where title=? ";
    private static final String SearchByID = "select  title from book where id=? ";
    private static final String INSERT_Books_SQL = "INSERT INTO book" +
            "  (title, subject, isbn, status,price,penalty) VALUES " +
            " ( ? , ? , ? , ? , ? , ?);";
    private static final String UPDATE_Books_SQL = "update book set status = ? where id = ?;";

    public void CreateTable() {
        try {
            Connection connection = MYDB.getConnection();
            Statement statement = connection.createStatement();
            statement.execute(booksTable);


        } catch (Exception e) {
            System.out.println(e);
            e.getStackTrace();
        }
    }


    public String searchByID(int id) throws Exception {
        String name = " ";
        try {
            Connection connection = MYDB.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SearchByID);
            preparedStatement.setInt(1, id);

            ResultSet rs = preparedStatement.executeQuery();
            boolean found = rs.next();


            if (found) {
                name = rs.getString("title");

            }


        } catch (SQLException e) {
            e.printStackTrace();

        }
        return name;
    }

    public int getIdByName(String name) {
        int CurrentId = 0;
        try {
            Connection connection = MYDB.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(GettingTitleId);
            preparedStatement.setString(1, name);

            ResultSet rs = preparedStatement.executeQuery();
            boolean found = rs.next();
            if (found) {
                CurrentId = rs.getInt(1);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        return CurrentId;
    }

    public void searchByName(String name) throws Exception {

        try {
            Connection connection = MYDB.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SearchByName);
            preparedStatement.setString(1, name);

            ResultSet rs = preparedStatement.executeQuery();
            boolean found = rs.next();


            if (found) {
                long penalty = rs.getLong("penalty");
                BookController.setChosenBookPenalty(penalty);


                long price = rs.getLong("price");
                BookController.setChosenBookPrice(price);


                boolean status = rs.getBoolean("status");
                BookController.setChosenBookStatus(status);

                int chosenBookId = rs.getInt(1);
                BookController.setChosenBookId(chosenBookId);

                updateStartBorrow();
            } else {
                System.out.println("This is not in the DataBase!");
            }


        } catch (SQLException e) {
            e.printStackTrace();

        }
    }


    public void insert_Books(Book book) {

        try {
            Connection connection = MYDB.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT_Books_SQL);
            preparedStatement.setString(1, book.getTitle());
            preparedStatement.setString(2, book.getSubject());
            preparedStatement.setLong(3, book.getIsbn());
            preparedStatement.setBoolean(4, book.isStatus());
            preparedStatement.setLong(5, book.getPrice());
            preparedStatement.setLong(6, book.getPenalty());
            preparedStatement.execute();
        } catch (Exception ex) {
            System.out.println(ex);
            ex.getStackTrace();
        }


    }

    public void updateStartBorrow() throws Exception {
        // Step 1: Establishing a Connection
        try {
            Connection connection = MYDB.getConnection();
            // Step 2:Create a statement using connection object
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_Books_SQL);
            preparedStatement.setBoolean(1, false);
            preparedStatement.setInt(2, BookController.getChosenBookId());

            // Step 3: Execute the query or update query
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Step 4: try-with-resource statement will auto close the connection.
    }

    public void updateEndBorrow() throws Exception {

        try {
            Connection connection = MYDB.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_Books_SQL);
            preparedStatement.setBoolean(1, true);
            preparedStatement.setInt(2, BookController.getChosenBookId());

            preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void Read() {
        try {
            Connection connection = MYDB.getConnection();

            // Step 2:Create a statement using connection object

            PreparedStatement preparedStatement = connection.prepareStatement(QUERY);

//            preparedStatement.setInt(1, 1);
            // System.out.println(preparedStatement);
            // Step 3: Execute the query or update query
            ResultSet rs = preparedStatement.executeQuery();

            // Step 4: Process the ResultSet object.
            while (rs.next()) {
                //     int id = rs.getInt("id");
                String title = rs.getString("title");
                String subject = rs.getString("subject");
                long isbn = rs.getLong("isbn");
                boolean status = rs.getBoolean("status");
                long price = rs.getLong("price");
                System.out.println(" Title: [" + title + "], Subject: [" + subject + "], ISBN: [" + isbn + "], Status: [" + status + "] , price: [" + price + "]");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
