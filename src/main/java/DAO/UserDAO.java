package DAO;

import Controller.BorrowController;
import Controller.UserController;
import Security.PassHash;
import Service.BookService;
import models.Book;
import models.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class UserDAO {


    private static final String userTable = "CREATE TABLE IF NOT EXISTS users(" +
            "id                        INT PRIMARY KEY" +
            ",full_name                VARCHAR," +
            "password                  VARCHAR " +
            "created_at                TIMESTAMP ," +
            "national_code             VARCHAR," +
            "status BOOLEAN NOT NULL CHECK (status IN (0,1))) ," +
            "budget                    INT8 )";
    private static final String QUERY = "select  * from users ";
    private static final String LOGIN = "select  id,full_name,password,budget from users where full_name=?  AND password=? ";
    private static final String INSERT_USERS_SQL = "INSERT INTO users" +
            "  (id,full_name, password, created_at, national_code,status,budget) VALUES " +
            " (null , ? , ? , ? , ? , ? , ?);";
    private static final String UPDATE_USERS_SQL = "update users set budget = ?  where id = ?;";

    User user;
    BorrowController borrowController;

    public UserDAO() {
        user = new User();
        borrowController = new BorrowController();
    }

    public void CreateTable() {
        try {

            Connection connection = MYDB.getConnection();
            Statement statement = connection.createStatement();
            statement.execute(userTable);


        } catch (Exception e) {
            System.out.println(e);
            e.getStackTrace();
        }
    }

    public void insert(User user) {

        try {
            Connection connection = MYDB.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT_USERS_SQL);
            preparedStatement.setString(1, user.getFull_name());
            preparedStatement.setString(2, user.getPassword());
            preparedStatement.setTimestamp(3, user.getCreated_at());
            preparedStatement.setString(4, user.getNational_code());
            preparedStatement.setBoolean(5, user.isStatus());
            preparedStatement.setLong(6, user.getBudget());

            preparedStatement.executeUpdate();

        } catch (Exception ex) {
            System.out.println(ex);
            ex.getStackTrace();
        }


    }

    public void updateRecord() throws Exception {
        long updatedBudget = borrowController.UpdatingUserBudget();
        try {
            Connection connection = MYDB.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_USERS_SQL);
            preparedStatement.setLong(1, updatedBudget);
            preparedStatement.setInt(2, UserController.getCurrentUserId());
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public void updateRecordWithPenalty() throws Exception {

        try {
            Connection connection = MYDB.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_USERS_SQL);
            preparedStatement.setLong(1,BorrowController.getCalculatedPenaltyBudget());
            preparedStatement.setInt(2, UserController.getCurrentUserId());
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void Read() {
        try {
            Connection connection = MYDB.getConnection();

            // Step 2:Create a statement using connection object
            PreparedStatement preparedStatement = connection.prepareStatement(QUERY);
            System.out.println(preparedStatement);
            // Step 3: Execute the query or update query
            ResultSet rs = preparedStatement.executeQuery();

            // Step 4: Process the ResultSet object.
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("full_name");
                String password = rs.getString("password");
                String created_at = rs.getString("created_at");
                long national_code = rs.getLong("national_code");
                boolean status = rs.getBoolean("status");
                long budget = rs.getLong("budget");
                System.out.println("id:{ " + id + "} ," + "  full_name: {" + name + "} ," + "  password: {" + password + "} ," + "  Created_at:{" + created_at + "} ," + "  national-Code:{ " + national_code + "} ," + "  status: {" + status + "} ," + "  budget: {" + budget + "}");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void login(String userName, String passWord) {
        try {


            String EecodedPass = PassHash.getMd5(passWord);

            Connection connection = MYDB.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(LOGIN);
            preparedStatement.setString(1, userName);
            preparedStatement.setString(2, EecodedPass);


            ResultSet rs;

            rs = preparedStatement.executeQuery();

            boolean found = rs.next();
            if (found) {

                SetCurrentUserInfo(rs);

            } else {
                System.out.println("Access is not possible");
            }


        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void SetCurrentUserInfo(ResultSet rs) throws SQLException {
        String currentUser = rs.getString(2);
        int id = rs.getInt("id");
        UserController.setCurrentUserId(id);
        UserController.setMyCurrentUser(currentUser);
        long Budget = rs.getLong("budget");
        UserController.setCurrentUserBudget(Budget);
        System.out.println("Welcome " + currentUser);
    }


}



