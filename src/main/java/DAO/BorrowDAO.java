package DAO;



import Controller.BorrowController;
import Controller.UserController;
import models.Borrow;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class BorrowDAO {
    private static final String CreateTable = "create table if not exist borrow(" +
            "id INT PRIMARY KEY," +
            "user_id int" +
            ",book_id int" +
            ",start VARCHAR " +
            ",end VARCHAR" +
            ",FOREIGN KEY (user_id) REFERENCES users(id)" +
            ",FOREIGN KEY (book_id) REFERENCES book(id)" +
            ") ";
    private static final String QUERY = "select  * from borrow where user_id=?";
    private static final String Delete = "delete from borrow where book_id in (select id from book where title=?)";
    private static final String CheckReturnQUERY = "select  end from borrow where user_id=? ";
    private static final String ReadDeadLineQuery = "select end  from borrow where book_id in (select id from book where title=?)";
    private static final String INSERT_barrow_SQL = "INSERT INTO borrow" +
            "  (user_id, book_id , start, end) VALUES " +
            " ( ? , ? , ? , ?);";
    private static final String UPDATE_barrow_SQL = "update Users set user_id = ? , book_id = ? where id = ?;";

    Borrow barrow;
    BookDAO bookDAO;

    public BorrowDAO() {
        barrow = new Borrow();
        bookDAO = new BookDAO();

    }

    public void createTable() {
        try {
            Connection connection = MYDB.getConnection();
            Statement statement = connection.createStatement();
            statement.execute(CreateTable);


        } catch (Exception e) {
            System.out.println(e);
            e.getStackTrace();
        }
    }

    public void InsertBarrowData(Borrow barrow) {

        try {
            Connection connection = MYDB.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT_barrow_SQL);
            preparedStatement.setInt(1, barrow.getUSERID());
            preparedStatement.setInt(2, barrow.getBOOKID());
            preparedStatement.setString(3, barrow.getStart());
            preparedStatement.setString(4, barrow.getEnd());
            preparedStatement.executeUpdate();
        } catch (Exception ex) {
            System.out.println(ex);
            ex.getStackTrace();
        }


    }

    public Date ReadDeadLineTime(String title) {
        Connection connection = MYDB.getConnection();
        Date deadLine = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(ReadDeadLineQuery);
            preparedStatement.setString(1, title);

            // Step 3: Execute the query or update query
            ResultSet rs = preparedStatement.executeQuery();
            boolean found = rs.next();
            // Step 4: Process the ResultSet object.
            if (found) {

                String end = rs.getString("end");
                deadLine =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(end);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return deadLine;
    }


    public void updateRecord(Borrow borrow) throws Exception {
        // Step 1: Establishing a Connection
        try {
            Connection connection = MYDB.getConnection();
            // Step 2:Create a statement using connection object
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_barrow_SQL);
            preparedStatement.setString(3, barrow.getStart());

            preparedStatement.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void DeleteRecord(String title) {
        try {
            Connection connection = MYDB.getConnection();
            // Step 2:Create a statement using connection object
            PreparedStatement preparedStatement = connection.prepareStatement(Delete);
            preparedStatement.setString(1, title);

            preparedStatement.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void ReadCurrentUserBorrows() {
        try {
            Connection connection = MYDB.getConnection();

            // Step 2:Create a statement using connection object
            PreparedStatement preparedStatement = connection.prepareStatement(QUERY);
            preparedStatement.setInt(1, UserController.getCurrentUserId());

            // Step 3: Execute the query or update query
            ResultSet rs = preparedStatement.executeQuery();

            // Step 4: Process the ResultSet object.
            while (rs.next()) {
//                int id = rs.getInt("id");
//                int user_id = rs.getInt("user_id");
                int book_id = rs.getInt("book_id");
                String bookTitle = bookDAO.searchByID(book_id);
                String start = rs.getString("start");
                Date end = rs.getDate("end");
                System.out.println(" Book_title:[" + bookTitle + "], start:[" + start + "], DeadLine[" + end + "]");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void CheckingEndDate() {
        try {
            Connection connection = MYDB.getConnection();


            PreparedStatement preparedStatement = connection.prepareStatement(CheckReturnQUERY);
            preparedStatement.setInt(1, UserController.getCurrentUserId());
            ResultSet rs = preparedStatement.executeQuery();
            boolean found = rs.next();
            // Step 4: Process the ResultSet object.
            if (found) {

                String end = rs.getString("end");
                Date ending = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(end);
                BorrowController.setCurrentBorrowEndDate(ending);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
