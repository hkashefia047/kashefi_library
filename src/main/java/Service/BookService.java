package Service;

import DAO.BookDAO;
import models.Book;

public class BookService {
    BookDAO bookDAO;
    public BookService() {
        bookDAO = new BookDAO();
    }
    public void insert(Book book){
        bookDAO.insert_Books(book);
    }
    public void readBooks(){
      bookDAO.Read();
    }
    public void updateBookBorrowingStatus(){
        try {
            bookDAO.updateStartBorrow();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void updateReturnBookStatus(){
        try {
            bookDAO.updateEndBorrow();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public  void SearchByName(String name) throws Exception {
         bookDAO.searchByName(name);
    }
    public int gettingIdByName(String name){
        return bookDAO.getIdByName(name);
    }
}
