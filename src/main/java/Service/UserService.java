package Service;

import DAO.UserDAO;
import models.User;

public class UserService {

    UserDAO userDAO;

    public UserService() {
        userDAO = new UserDAO();
    }


    public void insert(User user) {

        userDAO.insert(user);
    }

    public void read() {
        userDAO.Read();
    }

    public void update() {
        try {
            userDAO.updateRecord();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateWithPenalty(){
        try {
            userDAO.updateRecordWithPenalty();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void login(String username, String password) {
        userDAO.login(username, password);
    }
}
