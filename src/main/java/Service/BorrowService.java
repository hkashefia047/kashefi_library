package Service;


import DAO.BorrowDAO;
import models.Borrow;

import java.util.Date;

public class BorrowService {
    BorrowDAO borrowDAO;

    public BorrowService() {
        borrowDAO = new BorrowDAO();
    }

    public Date borrowDeadLine(String title) {
        return borrowDAO.ReadDeadLineTime(title);
    }

    public void insertBarrowData(Borrow borrow) {

        borrowDAO.InsertBarrowData(borrow);
    }

    public void read() {
        borrowDAO.ReadCurrentUserBorrows();
    }

    public void update(Borrow borrow) {
        try {
            borrowDAO.updateRecord(borrow);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteRecord(String title) {
        borrowDAO.DeleteRecord(title);
    }
}
