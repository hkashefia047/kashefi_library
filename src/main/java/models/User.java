package models;

import java.sql.Timestamp;

public class User {

    Integer id;
    String full_name;
    String password;
    Timestamp created_at;
    String national_code;
    boolean status;
    long Budget;


    public static  void a(){

    }

    public User() {
    }

    public User(String full_name, String password, Timestamp created_at, String national_code, boolean status, long budget) {
        this.full_name = full_name;
        this.password = password;
        this.created_at = created_at;
        this.national_code = national_code;
        this.status = status;
        this.Budget = budget;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Timestamp getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Timestamp created_at) {
        this.created_at = created_at;
    }

    public String getNational_code() {
        return national_code;
    }

    public void setNational_code(String national_code) {
        this.national_code = national_code;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public long getBudget() {
        return Budget;
    }

    public void setBudget(long budget) {
        Budget = budget;
    }
}