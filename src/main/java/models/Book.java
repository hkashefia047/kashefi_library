package models;

public class Book {

    String title;
    String subject;
    long isbn;
   boolean status;
   long price;
    long penalty;

    public Book() {
    }

    public Book(String title, String subject, long isbn, boolean status, long price, long penalty) {
        this.title = title;
        this.subject = subject;
        this.isbn = isbn;
        this.status = status;
        this.price = price;
        this.penalty = penalty;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public long getIsbn() {
        return isbn;
    }

    public void setIsbn(long isbn) {
        this.isbn = isbn;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public long getPenalty() {
        return penalty;
    }

    public void setPenalty(long penalty) {
        this.penalty = penalty;
    }
}
