package models;

import java.sql.*;

public class Borrow {
    int id;
    int USERID;
    int BOOKID;
    String start;
    String end;

    public Borrow() {
    }

    public Borrow(int USERID, int BOOKID, String start, String end) {
        this.USERID = USERID;
        this.BOOKID = BOOKID;
        this.start = start;
        this.end = end;
    }

    public int getUSERID() {
        return USERID;
    }

    public void setUSERID(int USERID) {
        this.USERID = USERID;
    }

    public int getBOOKID() {
        return BOOKID;
    }

    public void setBOOKID(int BOOKID) {
        this.BOOKID = BOOKID;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }
}
